package com.wtg.chart.metrics;

import com.wtg.chart.growth.Age2To20YearsChartDataBMIVsAge;
import com.wtg.chart.growth.Age2To20YearsChartDataStatureVsAge;
import com.wtg.chart.growth.Age2To20YearsChartDataWeightVsAge;
import com.wtg.chart.growth.ChartDataWeightVsStature;
import com.wtg.chart.growth.Gender;
import com.wtg.chart.growth.GrowthChartData;
import com.wtg.chart.growth.GrowthRecord;
import com.wtg.chart.growth.InfantChartDataHeadCircumferenceVsAge;
import com.wtg.chart.growth.InfantChartDataLengthVsAge;
import com.wtg.chart.growth.InfantChartDataWeightVsAge;
import com.wtg.chart.growth.InfantChartDataWeightVsLength;
import com.wtg.chart.model.GrowthGraphRecord;
import com.wtg.chart.model.PercentileData;

public class USCDCGrowthPercentileCalculator extends AbstractUSCDCGrowthPercentilesCalculator
{

    public USCDCGrowthPercentileCalculator(final Gender gender, final String dob)
    {
	super(gender, dob);

    }

    @Override
    protected void updateHeadCircumferencePercentile(final GrowthGraphRecord record)
    {
	GrowthChartData chartData = null;
	chartData = InfantChartDataHeadCircumferenceVsAge.getInstance();
	assert chartData != null;
	final GrowthRecord matchingRecord = chartData.getGrowthRecord(record.getAgeInMonths(), getGender(), false);

	final PercentileData headCircumferencepercentile = WTGZScoreAndPercentileCaluclator.getPercentileValue(
	        matchingRecord, record.getHeadCircumference());
	headCircumferencepercentile.getStringPercentileValue();

    }

    @Override
    protected void updateInfantHeightPercentile(final GrowthGraphRecord record)
    {

	GrowthChartData chartData = null;
	chartData = InfantChartDataLengthVsAge.getInstance();
	assert chartData != null;
	final GrowthRecord matchingRecord = chartData.getGrowthRecord(record.getAgeInMonths(), getGender(), false);

	final PercentileData heightPercentile = WTGZScoreAndPercentileCaluclator.getPercentileValue(matchingRecord,
	        record.getHeight());
	final String hp = heightPercentile.getStringPercentileValue();
	record.setHeightPercentile(hp);
    }

    @Override
    protected void updateInfantWeightPercentile(final GrowthGraphRecord record)
    {

	GrowthChartData chartData = null;
	chartData = InfantChartDataWeightVsAge.getInstance();
	assert chartData != null;
	final GrowthRecord matchingRecord = chartData.getGrowthRecord(record.getAgeInMonths(), getGender(), false);

	final PercentileData weightPercentile = WTGZScoreAndPercentileCaluclator.getPercentileValue(matchingRecord,
	        record.getWeight());
	final String wp = weightPercentile.getStringPercentileValue();
	record.setWeightPercentile(wp);

    }

    @Override
    protected void updateInfantWeightStaturePercentile(final GrowthGraphRecord record)
    {

	GrowthChartData chartData = null;
	chartData = InfantChartDataWeightVsLength.getInstance();
	assert chartData != null;
	final GrowthRecord matchingRecord = chartData.getGrowthRecord(record.getHeight(), getGender(), true);

	final PercentileData wtStaturePercentile = WTGZScoreAndPercentileCaluclator.getPercentileValue(matchingRecord,
	        record.getWeight());
	final String wsp = wtStaturePercentile.getStringPercentileValue();
	record.setWeightStaturePercentile(wsp);

    }

    @Override
    protected void update2To20YrsBMIPercentile(final GrowthGraphRecord record)
    {

	GrowthChartData chartData = null;
	chartData = Age2To20YearsChartDataBMIVsAge.getInstance();
	assert chartData != null;
	final GrowthRecord matchingRecord = chartData.getGrowthRecord(record.getAgeInMonths(), getGender(), false);

	final PercentileData age2To20YearsBMIPercentileByAge = WTGZScoreAndPercentileCaluclator.getPercentileValue(
	        matchingRecord, Double.valueOf(record.getBMI()));
	final String bmiPercentile = age2To20YearsBMIPercentileByAge.getStringPercentileValue();
	record.setBMIPercentile(bmiPercentile);

    }

    @Override
    protected void updateAge2To20YearsWeightPercentileByAge(final GrowthGraphRecord record)
    {

	GrowthChartData chartData = null;
	chartData = Age2To20YearsChartDataWeightVsAge.getInstance();
	assert chartData != null;
	final GrowthRecord matchingRecord = chartData.getGrowthRecord(record.getAgeInMonths(), getGender(), false);

	final PercentileData weightPercentile = WTGZScoreAndPercentileCaluclator.getPercentileValue(matchingRecord,
	        record.getWeight());
	record.setWeightPercentile(weightPercentile.getStringPercentileValue());
    }

    @Override
    protected void updateAge2To20YearsHeightPercentileByAge(final GrowthGraphRecord record)
    {

	GrowthChartData chartData = null;
	chartData = Age2To20YearsChartDataStatureVsAge.getInstance();
	assert chartData != null;
	final GrowthRecord matchingRecord = chartData.getGrowthRecord(record.getAgeInMonths(), getGender(), false);

	final PercentileData heightPercentile = WTGZScoreAndPercentileCaluclator.getPercentileValue(matchingRecord,
	        record.getHeight());
	final String hp = heightPercentile.getStringPercentileValue();
	record.setHeightPercentile(hp);
    }

    @Override
    protected void calculateAgePreschoolerWeightPercentileByHeight(final GrowthGraphRecord record)
    {

	GrowthChartData chartData = null;
	chartData = ChartDataWeightVsStature.getInstance();
	assert chartData != null;
	final GrowthRecord matchingRecord = chartData.getGrowthRecord(record.getHeight(), getGender(), true);

	final PercentileData wtStaturePercentile = WTGZScoreAndPercentileCaluclator.getPercentileValue(matchingRecord,
	        record.getWeight());
	final String percentile = wtStaturePercentile.getStringPercentileValue();
	record.setWeightStaturePercentile(percentile);

    }

}
