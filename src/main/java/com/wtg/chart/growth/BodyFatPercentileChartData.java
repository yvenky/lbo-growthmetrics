package com.wtg.chart.growth;

import com.wtg.chart.ChartType;

public class BodyFatPercentileChartData extends GrowthChartData
{
    private static BodyFatPercentileChartData instance;

    private BodyFatPercentileChartData()
    {
    }

    /**
     * returns singleInstance.
     * 
     * @return
     */
    public static GrowthChartData getInstance()
    {
	if (instance == null)
	{
	    synchronized (Age2To20YearsChartDataBMIVsAge.class)
	    {
		if (instance == null)
		{
		    instance = new BodyFatPercentileChartData();
		}
	    }
	}
	return instance;
    }

    @Override
    ChartType getChartType()
    {
	// TODO Auto-generated method stub
	return ChartType.BodyFat;
    }

}
