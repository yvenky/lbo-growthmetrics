package com.wtg.chart.model;

public class GrowthPercentileData implements GrowthPercentile
{

    private PercentileData bmiPercentileByAge;
    private PercentileData headCircumferencePercentileByAge;
    private PercentileData heightPercentileByge;
    private PercentileData weightPercentileByAge;
    private PercentileData weightPercentileByHeight;

    @Override
    public PercentileData getBMIPercentileByAge()
    {
	return bmiPercentileByAge;
    }

    public void setBMIPercentileByAge(final PercentileData bMI_Percentile_By__Age)
    {
	this.bmiPercentileByAge = bMI_Percentile_By__Age;
    }

    @Override
    public PercentileData getHCPercentileByAge()
    {
	return headCircumferencePercentileByAge;
    }

    public void setHCPercentileByAge(final PercentileData headCircumference_percentile_By_Age)
    {
	this.headCircumferencePercentileByAge = headCircumference_percentile_By_Age;
    }

    @Override
    public PercentileData getHeightPercentileByAge()
    {
	return heightPercentileByge;
    }

    public void setHeightPercentileByByAge(final PercentileData height_percentile_By_Age)
    {
	this.heightPercentileByge = height_percentile_By_Age;
    }

    @Override
    public PercentileData getWeightPercentileByAge()
    {
	return weightPercentileByAge;
    }

    public void setWeightPercentileByAge(final PercentileData weight_percentile_By_Age)
    {
	this.weightPercentileByAge = weight_percentile_By_Age;
    }

    @Override
    public PercentileData getWeightPercentileByHeight()
    {
	return weightPercentileByHeight;
    }

    public void setWeightPercentileByHeight(final PercentileData weight_percentile_By_Height)
    {
	this.weightPercentileByHeight = weight_percentile_By_Height;
    }

}
