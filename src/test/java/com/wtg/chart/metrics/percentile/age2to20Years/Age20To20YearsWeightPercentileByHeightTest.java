package com.wtg.chart.metrics.percentile.age2to20Years;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.wtg.chart.model.PercentileData;
import com.wtg.chart.model.PercentileRange;

public class Age20To20YearsWeightPercentileByHeightTest extends
		Age2To20YearsPercentileDataTest 
		{

	@Test
	public void assertWeightPercentileTest()
	{
		PercentileData weightPercentileData = testRecord.getGrowthPercentile().getWeightPercentileByHeight();
		assert weightPercentileData!= null;
		assertEquals(35.63, weightPercentileData.getPercentile(), 0);
		assertEquals(PercentileRange.InRange, weightPercentileData.getPercentileRange());
	}
}
