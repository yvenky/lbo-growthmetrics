package com.wtg.chart.metrics.percentile.infant;

import static org.junit.Assert.*;

import org.junit.Test;

import com.wtg.chart.model.PercentileData;
import com.wtg.chart.model.PercentileRange;

public class InfantWeightPercentileTest extends InfantPercentileDataTest {

	@Test
	public void test()
	{
		PercentileData wtPercentileTest = testRecord.getGrowthPercentile().getWeightPercentileByAge();
		assert wtPercentileTest!= null;
		assertEquals(46.58, wtPercentileTest.getPercentile(), 0);
		assertEquals(PercentileRange.InRange, wtPercentileTest.getPercentileRange());
	}

}
