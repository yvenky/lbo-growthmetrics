package com.wtg.chart.metrics.percentile;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.wtg.chart.growth.Gender;
import com.wtg.chart.metrics.USCDCGrowthPercentileCalculator;
import com.wtg.chart.metrics.WTGGrowthPercentileCalculator;
import com.wtg.chart.model.GrowthPercentile;
import com.wtg.chart.model.GrowthPercentileData;
import com.wtg.chart.model.PercentileData;
import com.wtg.chart.model.PercentileRange;
import com.wtg.chart.model.PhysicalGrowth;

public class Age2To20YearsPercentilesTest
{

    private List<PhysicalGrowth> recordsList = new ArrayList<PhysicalGrowth>();

    private GrowthPercentile minimumGrowthPercentileData = new GrowthPercentileData();

    private GrowthPercentile maximumGrowthPercentileData = new GrowthPercentileData();

    @Before
    public void setup()
    {
	loadInfantMinimumGrowthObject();
	WTGGrowthPercentileCalculator percentileCaluclator = new USCDCGrowthPercentileCalculator(Gender.MALE,
	        "11/11/2002");
	percentileCaluclator.updatePercentilesForRecords(recordsList);
	minimumGrowthPercentileData = recordsList.get(0).getGrowthPercentile();

	loadInfantMaximumGrowthObject();
	percentileCaluclator = new USCDCGrowthPercentileCalculator(Gender.MALE, "11/11/2002");
	percentileCaluclator.updatePercentilesForRecords(recordsList);
	maximumGrowthPercentileData = recordsList.get(0).getGrowthPercentile();

    }

    private void loadInfantMinimumGrowthObject()
    {

	recordsList = new ArrayList<PhysicalGrowth>();
	final PhysicalGrowth record1 = new PhysicalGrowth();
	record1.setWeight(12.38209025);
	record1.setHeight(89.99171445);
	record1.setDate("11/11/2005");
	recordsList.add(record1);
    }

    private void loadInfantMaximumGrowthObject()
    {

	recordsList = new ArrayList<PhysicalGrowth>();
	final PhysicalGrowth record1 = new PhysicalGrowth();
	record1.setWeight(19.48794495);
	record1.setHeight(117.831386);
	record1.setDate("12/11/2006");
	recordsList.add(record1);

    }

    @Ignore
    public void testBMIPercentileWithMinimumValues()
    {
	final PercentileData bmiPercentile = minimumGrowthPercentileData.getBMIPercentileByAge();
	assert bmiPercentile != null;
	assertEquals(25.4, bmiPercentile.getPercentile(), 0);
	assertEquals(PercentileRange.InRange, bmiPercentile.getPercentileRange());
    }

    @Test
    public void testHeightPercentileWithMinimumValues()
    {
	final PercentileData heightPercentileData = minimumGrowthPercentileData.getHeightPercentileByAge();
	assert heightPercentileData != null;
	assertEquals(9.07, heightPercentileData.getPercentile(), 0);
	assertEquals(PercentileRange.InRange, heightPercentileData.getPercentileRange());
    }

    @Test
    public void testWeightPercentileWithMinimumValues()
    {
	final PercentileData weightPercentileData = minimumGrowthPercentileData.getWeightPercentileByAge();
	assert weightPercentileData != null;
	assertEquals(8.56, weightPercentileData.getPercentile(), 0);
	assertEquals(PercentileRange.InRange, weightPercentileData.getPercentileRange());
    }

    @Test
    public void testWeightPercentileByHeightWithMinimumValues()
    {
	final PercentileData weightPercentileDataByHeight = minimumGrowthPercentileData.getWeightPercentileByHeight();
	assert weightPercentileDataByHeight != null;
	assertEquals(25.78, weightPercentileDataByHeight.getPercentile(), 0);
	assertEquals(PercentileRange.InRange, weightPercentileDataByHeight.getPercentileRange());
    }

    @Ignore
    public void testBMIPercentileWithMaximumValues()
    {
	final PercentileData bmiPercentile = minimumGrowthPercentileData.getBMIPercentileByAge();
	assert bmiPercentile != null;
	assertEquals(25.40, bmiPercentile.getPercentile(), 0);
	assertEquals(PercentileRange.InRange, bmiPercentile.getPercentileRange());
    }

    @Test
    public void testHeightPercentileWithMaximumValues()
    {
	final PercentileData heightPercentileData = maximumGrowthPercentileData.getHeightPercentileByAge();
	assert heightPercentileData != null;
	assertEquals(99.98, heightPercentileData.getPercentile(), 0);
	assertEquals(PercentileRange.InRange, heightPercentileData.getPercentileRange());
    }

    @Test
    public void testWeightPercentileWithMaximumValues()
    {
	final PercentileData weightPercentileData = maximumGrowthPercentileData.getWeightPercentileByAge();
	assert weightPercentileData != null;
	assertEquals(90.72, weightPercentileData.getPercentile(), 0);
	assertEquals(PercentileRange.InRange, weightPercentileData.getPercentileRange());
    }

    @Test
    public void testWeightPercentileByHeightWithMaximumValues()
    {
	final PercentileData weightPercentileDataByHeight = maximumGrowthPercentileData.getWeightPercentileByHeight();
	assert weightPercentileDataByHeight != null;
	assertEquals(13.21, weightPercentileDataByHeight.getPercentile(), 0);
	assertEquals(PercentileRange.InRange, weightPercentileDataByHeight.getPercentileRange());
    }

}
