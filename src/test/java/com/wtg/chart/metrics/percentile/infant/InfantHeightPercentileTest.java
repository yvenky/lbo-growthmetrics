package com.wtg.chart.metrics.percentile.infant;

import static org.junit.Assert.*;

import org.junit.Test;

import com.wtg.chart.model.PercentileData;
import com.wtg.chart.model.PercentileRange;

public class InfantHeightPercentileTest extends InfantPercentileDataTest {

	@Test
	public void test()
	{
		PercentileData htPercentileTest = testRecord.getGrowthPercentile().getHeightPercentileByAge();
		assert htPercentileTest!= null;
		assertEquals(66.91, htPercentileTest.getPercentile(), 0);
		assertEquals(PercentileRange.InRange, htPercentileTest.getPercentileRange());
	}

}
