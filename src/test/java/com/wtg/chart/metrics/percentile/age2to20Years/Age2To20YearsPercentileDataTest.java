package com.wtg.chart.metrics.percentile.age2to20Years;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;

import com.wtg.chart.growth.Gender;
import com.wtg.chart.metrics.AgeCalculatorUtil;
import com.wtg.chart.metrics.USCDCGrowthPercentileCalculator;
import com.wtg.chart.model.PhysicalGrowth;

public class Age2To20YearsPercentileDataTest
{

    PhysicalGrowth testRecord = new PhysicalGrowth();
    String dateOfBirth = "11/11/2003";
    List<PhysicalGrowth> physicalGrowthRecords = new ArrayList<PhysicalGrowth>();

    public void createGrowthSketchableRecord(final double wt, final double ht, final String recordedDate)
    {
	testRecord = new PhysicalGrowth();
	testRecord.setWeight(wt);
	testRecord.setHeight(ht);
	testRecord.setDate(recordedDate);

	final double ageInMonths = AgeCalculatorUtil.getCurrentAgeInMonths(dateOfBirth, testRecord.getRecordedDate());

	testRecord.setAgeInMonths(ageInMonths);
	physicalGrowthRecords.add(testRecord);
    }

    @Before
    public void setup()
    {

	createGrowthSketchableRecord(15.0916281, 99.65875031, "11/27/2007");
	final USCDCGrowthPercentileCalculator calculator = new USCDCGrowthPercentileCalculator(Gender.MALE, dateOfBirth);
	calculator.updatePercentilesForRecords(physicalGrowthRecords);

    }

}
