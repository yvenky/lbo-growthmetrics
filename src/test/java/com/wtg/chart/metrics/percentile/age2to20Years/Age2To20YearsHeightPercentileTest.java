package com.wtg.chart.metrics.percentile.age2to20Years;

import static org.junit.Assert.*;

import org.junit.Test;

import com.wtg.chart.model.PercentileData;
import com.wtg.chart.model.PercentileRange;

public class Age2To20YearsHeightPercentileTest extends Age2To20YearsPercentileDataTest {

	@Test
	public void assertWeightPercentileTest()
	{
		PercentileData heightPercentileData = testRecord.getGrowthPercentile().getHeightPercentileByAge();
		assert heightPercentileData!= null;
		assertEquals(24.90, heightPercentileData.getPercentile(), 0);
		assertEquals(PercentileRange.InRange, heightPercentileData.getPercentileRange());
	}

}
