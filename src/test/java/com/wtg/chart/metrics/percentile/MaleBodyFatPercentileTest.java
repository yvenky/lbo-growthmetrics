package com.wtg.chart.metrics.percentile;

import org.junit.Before;
import org.junit.Test;

import com.wtg.chart.growth.Gender;
import com.wtg.chart.metrics.USCDCGrowthPercentileCalculator;
import com.wtg.chart.metrics.WTGGrowthPercentileCalculator;
import com.wtg.chart.model.PhysicalGrowth;

public class MaleBodyFatPercentileTest
{
    private PhysicalGrowth growthRecord = null;
    WTGGrowthPercentileCalculator calculator = null;

    @Before
    public void setUp()
    {
	growthRecord = new PhysicalGrowth();
	calculator = new USCDCGrowthPercentileCalculator(Gender.MALE, "1/1/1990");
    }

    @Test
    public void test3Pc()
    {
	growthRecord.setDate("1/1/1995");
	growthRecord.setHeight(77);
	growthRecord.setWeight(8.73);
	calculator.updatePercentiles(growthRecord);
	System.out.println(growthRecord.getBodyFatPercent());
    }

}
