package com.wtg.chart.metrics.percentile.age2to20Years;

import static org.junit.Assert.assertEquals;

import org.junit.Ignore;
import org.junit.Test;

import com.wtg.chart.model.PercentileData;
import com.wtg.chart.model.PercentileRange;
import com.wtg.chart.model.WeightStatus;

public class BMIAge2to20YearsPercentileTest extends Age2To20YearsPercentileDataTest
{

    @Ignore
    public void assertBMIValue()
    {
	assertEquals(15.20, Double.valueOf(testRecord.getBMI()), 0);
    }

    @Test
    public void assertBMIWeightStatus()
    {
	assertEquals(WeightStatus.Underweight, testRecord.getBmiWeightStatus());
    }

    @Ignore
    public void assertBodyFatPercent()
    {
	assertEquals(17.92, Double.valueOf(testRecord.getBodyFatPercent()), 0);
    }

    @Ignore
    public void assertBMIPercent()
    {

	final PercentileData bmiPercentileData = testRecord.getGrowthPercentile().getBMIPercentileByAge();
	assert bmiPercentileData != null;
	assertEquals(34.63, bmiPercentileData.getPercentile(), 0);
	assertEquals(PercentileRange.InRange, bmiPercentileData.getPercentileRange());
    }

}
