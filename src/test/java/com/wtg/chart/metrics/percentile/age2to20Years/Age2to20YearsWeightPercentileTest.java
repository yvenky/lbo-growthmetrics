package com.wtg.chart.metrics.percentile.age2to20Years;

import static org.junit.Assert.*;

import org.junit.Test;

import com.wtg.chart.model.PercentileData;
import com.wtg.chart.model.PercentileRange;

public class Age2to20YearsWeightPercentileTest extends Age2To20YearsPercentileDataTest {

	

	@Test
	public void assertWeightPercentileTest()
	{
		PercentileData weightPercentileData = testRecord.getGrowthPercentile().getWeightPercentileByAge();
		assert weightPercentileData!= null;
		assertEquals(24.93, weightPercentileData.getPercentile(), 0);
		assertEquals(PercentileRange.InRange, weightPercentileData.getPercentileRange());
	}
	
}
