package com.wtg.chart.metrics.percentile;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.wtg.chart.growth.Gender;
import com.wtg.chart.metrics.USCDCGrowthPercentileCalculator;
import com.wtg.chart.metrics.WTGGrowthPercentileCalculator;
import com.wtg.chart.model.GrowthGraphRecord;
import com.wtg.chart.model.GrowthPercentile;
import com.wtg.chart.model.GrowthPercentileData;
import com.wtg.chart.model.PercentileData;
import com.wtg.chart.model.PercentileRange;
import com.wtg.chart.model.PhysicalGrowth;

public class InfantPercentileTest
{

    private List<GrowthGraphRecord> recordsList;

    private GrowthPercentile minimumGrowthPercentileData = new GrowthPercentileData();

    private GrowthPercentile maximumGrowthPercentileData = new GrowthPercentileData();

    @Before
    public void setup()
    {
	loadInfantMinimumGrowthObject();
	WTGGrowthPercentileCalculator percentileCaluclator = new USCDCGrowthPercentileCalculator(Gender.MALE,
	        "11/11/2002");
	percentileCaluclator.updatePercentilesForRecords(recordsList);
	minimumGrowthPercentileData = recordsList.get(0).getGrowthPercentile();

	loadInfantMaximumGrowthObject();
	percentileCaluclator = new USCDCGrowthPercentileCalculator(Gender.MALE, "11/11/2002");
	percentileCaluclator.updatePercentilesForRecords(recordsList);
	maximumGrowthPercentileData = recordsList.get(0).getGrowthPercentile();

    }

    private void loadInfantMinimumGrowthObject()
    {

	recordsList = new ArrayList<GrowthGraphRecord>();
	final PhysicalGrowth record1 = new PhysicalGrowth();
	record1.setWeight(3.530203168);
	record1.setHeadCircumference(31.48762321);
	record1.setHeight(48.1893738140463);
	record1.setDate("11/12/2002");
	recordsList.add(record1);
    }

    private void loadInfantMaximumGrowthObject()
    {

	recordsList = new ArrayList<GrowthGraphRecord>();
	final PhysicalGrowth record1 = new PhysicalGrowth();
	record1.setWeight(13.31277633);
	record1.setHeadCircumference(52.57205364);
	record1.setHeight(81.2499031578935);
	record1.setDate("12/11/2003");
	recordsList.add(record1);

    }

    @Ignore
    public void testHCPercentileWithMinimumValues()
    {
	final PercentileData hcData = minimumGrowthPercentileData.getHCPercentileByAge();
	assert hcData != null;
	assertEquals(3.0, hcData.getPercentile(), 0);
	assertEquals(PercentileRange.InRange, hcData.getPercentileRange());
    }

    @Ignore
    public void testHeightPercentileWithMinimumValues()
    {
	final PercentileData heightPercentileData = minimumGrowthPercentileData.getHeightPercentileByAge();
	assert heightPercentileData != null;
	assertEquals(25.0, heightPercentileData.getPercentile(), 0);
	assertEquals(PercentileRange.InRange, heightPercentileData.getPercentileRange());
    }

    @Ignore
    public void testWeightPercentileWithMinimumValues()
    {
	final PercentileData weightPercentileData = minimumGrowthPercentileData.getWeightPercentileByAge();
	assert weightPercentileData != null;
	assertEquals(50.0, weightPercentileData.getPercentile(), 0);
	assertEquals(PercentileRange.InRange, weightPercentileData.getPercentileRange());
    }

    @Test
    public void testWeightPercentileByHeightWithMinimumValues()
    {
	final PercentileData weightPercentileDataByHeight = minimumGrowthPercentileData.getWeightPercentileByHeight();
	assert weightPercentileDataByHeight != null;
	assertEquals(99.34, weightPercentileDataByHeight.getPercentile(), 0);
	assertEquals(PercentileRange.InRange, weightPercentileDataByHeight.getPercentileRange());
    }

    @Test
    public void testHCPercentileWithMaximumValues()
    {
	final PercentileData hcData = maximumGrowthPercentileData.getHCPercentileByAge();
	assert hcData != null;
	assertEquals(100.0, hcData.getPercentile(), 0);
	assertEquals(PercentileRange.InRange, hcData.getPercentileRange());
    }

    @Test
    public void testHeightPercentileWithMaximumValues()
    {
	final PercentileData heightPercentileData = maximumGrowthPercentileData.getHeightPercentileByAge();
	assert heightPercentileData != null;
	assertEquals(92.80, heightPercentileData.getPercentile(), 0);
	assertEquals(PercentileRange.InRange, heightPercentileData.getPercentileRange());
    }

    @Test
    public void testWeightPercentileWithMaximumValues()
    {
	final PercentileData weightPercentileData = maximumGrowthPercentileData.getWeightPercentileByAge();
	assert weightPercentileData != null;
	assertEquals(97.92, weightPercentileData.getPercentile(), 0);
	assertEquals(PercentileRange.InRange, weightPercentileData.getPercentileRange());
    }

    @Test
    public void testWeightPercentileByHeightWithMaximumValues()
    {
	final PercentileData weightPercentileDataByHeight = maximumGrowthPercentileData.getWeightPercentileByHeight();
	assert weightPercentileDataByHeight != null;
	assertEquals(99.48, weightPercentileDataByHeight.getPercentile(), 0);
	assertEquals(PercentileRange.InRange, weightPercentileDataByHeight.getPercentileRange());
    }

}