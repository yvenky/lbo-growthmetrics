package com.wtg.chart.metrics.percentile.infant;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.wtg.chart.model.PercentileData;
import com.wtg.chart.model.PercentileRange;

public class InfantHeadCirumferencePercentileTest extends InfantPercentileDataTest
{

    @Test
    public void test()
    {
	final PercentileData hcPercentileDataTest = testRecord.getGrowthPercentile().getHCPercentileByAge();
	assert hcPercentileDataTest != null;
	// assertEquals(50.97, hcPercentileDataTest.getPercentile(), 0);
	assertEquals(PercentileRange.InRange, hcPercentileDataTest.getPercentileRange());
    }
}
