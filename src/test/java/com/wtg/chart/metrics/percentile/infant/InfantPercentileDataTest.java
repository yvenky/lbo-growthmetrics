package com.wtg.chart.metrics.percentile.infant;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;

import com.wtg.chart.growth.Gender;
import com.wtg.chart.metrics.AgeCalculatorUtil;
import com.wtg.chart.metrics.USCDCGrowthPercentileCalculator;
import com.wtg.chart.model.GrowthGraphRecord;
import com.wtg.chart.model.PhysicalGrowth;

public class InfantPercentileDataTest
{

    PhysicalGrowth testRecord = new PhysicalGrowth();
    String dateOfBirth = "11/11/2003";
    List<GrowthGraphRecord> physicalGrowthRecords = new ArrayList<GrowthGraphRecord>();

    public void createGrowthSketchableRecord(final double wt, final double ht, final String recordedDate,
	    final double hc)
    {
	testRecord = new PhysicalGrowth();
	testRecord.setWeight(wt);
	testRecord.setHeight(ht);
	testRecord.setDate(recordedDate);
	testRecord.setHeadCircumference(hc);

	final double ageInMonths = AgeCalculatorUtil.getCurrentAgeInMonths(dateOfBirth, testRecord.getRecordedDate());

	testRecord.setAgeInMonths(ageInMonths);
	physicalGrowthRecords.add(testRecord);
    }

    @Before
    public void setup()
    {

	createGrowthSketchableRecord(11.69971783, 83.8626908671008, "05/27/2005", 47.89486804);
	final USCDCGrowthPercentileCalculator calculator = new USCDCGrowthPercentileCalculator(Gender.MALE, dateOfBirth);
	calculator.updatePercentilesForRecords(physicalGrowthRecords);

    }

}
