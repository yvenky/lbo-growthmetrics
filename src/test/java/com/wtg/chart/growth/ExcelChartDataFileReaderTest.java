package com.wtg.chart.growth;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.InputStream;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

import com.wtg.chart.ChartType;

public class ExcelChartDataFileReaderTest
{

    private ExcelChartDataFileReader fileReader = null;

    @Before
    public void setUp()
    {
	fileReader = new ExcelChartDataFileReader();
    }

    @Test
    public void testGetBmiAge2To20YrsFileStream()
    {
	final InputStream stream = fileReader.getExcelInputStream(ChartType.AgeFor2To20YearsChartBMIVsAge
	        .getGrowthDataFileName());
	assertNotNull(stream);
    }

    @Test
    public void testGetBmiAge2To20YrsFileContent()
    {
	final Collection<GrowthRecord> growthRecordList = fileReader
	        .getGrowthRecordDataList(ChartType.AgeFor2To20YearsChartBMIVsAge);
	assertNotNull(growthRecordList);
    }

    @Test
    public void testGetHeadCircumferenceAgeFileStream()
    {
	final InputStream stream = fileReader.getExcelInputStream(ChartType.InfantChartHeadCircumferenceVsAge
	        .getGrowthDataFileName());
	assertNotNull(stream);
    }

    @Test
    public void testGetHeadCircumferenceAgeFileContent()
    {
	final Collection<GrowthRecord> growthRecordList = fileReader
	        .getGrowthRecordDataList(ChartType.InfantChartHeadCircumferenceVsAge);
	assertNotNull(growthRecordList);
	assertEquals(ChartType.InfantChartHeadCircumferenceVsAge.getNumberOfGrowthDataRows(), growthRecordList.size());
    }

    @Test
    public void testGetInfantChartLengthVsAgeFileStream()
    {
	final InputStream stream = fileReader.getExcelInputStream(ChartType.InfantChartLengthVsAge
	        .getGrowthDataFileName());
	assertNotNull(stream);
    }

    @Test
    public void testGetInfantChartLengthVsAgeFileContent()
    {
	final Collection<GrowthRecord> growthRecordList = fileReader
	        .getGrowthRecordDataList(ChartType.InfantChartLengthVsAge);
	assertNotNull(growthRecordList);
	assertEquals(ChartType.InfantChartLengthVsAge.getNumberOfGrowthDataRows(), growthRecordList.size());
    }

    @Test
    public void testGetStatureAge2To20YrsFileStream()
    {
	final InputStream stream = fileReader.getExcelInputStream(ChartType.Age2To20YearChartStatureVsAge
	        .getGrowthDataFileName());
	assertNotNull(stream);
    }

    @Test
    public void testGetStatureAge2To20YrsFileContent()
    {
	final Collection<GrowthRecord> growthRecordList = fileReader
	        .getGrowthRecordDataList(ChartType.Age2To20YearChartStatureVsAge);
	assertNotNull(growthRecordList);
	assertEquals(ChartType.Age2To20YearChartStatureVsAge.getNumberOfGrowthDataRows(), growthRecordList.size());
    }

    @Test
    public void testGetAgeFor2To20YearsChartWeightVsAgeFileStream()
    {
	final InputStream stream = fileReader.getExcelInputStream(ChartType.AgeFor2To20YearsChartWeightVsAge
	        .getGrowthDataFileName());
	assertNotNull(stream);
    }

    @Test
    public void testWeightAge2To20YearsFileContent()
    {
	final Collection<GrowthRecord> growthRecordList = fileReader
	        .getGrowthRecordDataList(ChartType.AgeFor2To20YearsChartWeightVsAge);
	assertNotNull(growthRecordList);

    }

    @Test
    public void testWeightLengthInfantFileStream()
    {
	final InputStream stream = fileReader.getExcelInputStream(ChartType.InfantChartWeightVsLength
	        .getGrowthDataFileName());
	assertNotNull(stream);
    }

    @Test
    public void testWeightLengthInfantFileContent()
    {
	final Collection<GrowthRecord> growthRecordList = fileReader
	        .getGrowthRecordDataList(ChartType.InfantChartWeightVsLength);
	assertNotNull(growthRecordList);
	assertEquals(ChartType.InfantChartWeightVsLength.getNumberOfGrowthDataRows(), growthRecordList.size());
    }

    @Test
    public void testAge2To20YearChartStatureVsAgeFileStream()
    {
	final InputStream stream = fileReader.getExcelInputStream(ChartType.Age2To20YearChartStatureVsAge
	        .getGrowthDataFileName());
	assertNotNull(stream);

    }

    @Test
    public void testAge2To20YearChartStatureVsAgeFileContent()
    {
	final Collection<GrowthRecord> growthRecordList = fileReader
	        .getGrowthRecordDataList(ChartType.Age2To20YearChartStatureVsAge);
	assertNotNull(growthRecordList);
	assertEquals(ChartType.Age2To20YearChartStatureVsAge.getNumberOfGrowthDataRows(), growthRecordList.size());

    }

    @Test
    public void testInfantChartWeightVsAgeFileStream()
    {
	final InputStream stream = fileReader.getExcelInputStream(ChartType.InfantChartWeightVsAge
	        .getGrowthDataFileName());
	assertNotNull(stream);
    }

    @Test
    public void testWeightVsAgeInfantFileContent()
    {
	final Collection<GrowthRecord> growthRecordList = fileReader
	        .getGrowthRecordDataList(ChartType.InfantChartWeightVsAge);
	assertNotNull(growthRecordList);
	assertEquals(ChartType.InfantChartWeightVsAge.getNumberOfGrowthDataRows(), growthRecordList.size());

    }

    @Test
    public void testBodyFatPercentileFileContent()
    {
	final Collection<GrowthRecord> growthRecordList = fileReader.getGrowthRecordDataList(ChartType.BodyFat);
	assertNotNull(growthRecordList);
	assertEquals(ChartType.BodyFat.getNumberOfGrowthDataRows(), growthRecordList.size());

    }

}
