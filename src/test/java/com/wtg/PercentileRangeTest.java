package com.wtg;

import org.junit.Assert;
import org.junit.Test;

import com.wtg.chart.model.PercentileRange;

public class PercentileRangeTest
{

    PercentileRange range = null;

    @Test
    public void testInRange()
    {
	range = PercentileRange.InRange;
	try
	{
	    range.getUIDisplayString();
	    Assert.fail("Should recieve exception");
	}
	catch (final RuntimeException re)
	{
	    // expected
	}

    }

}
